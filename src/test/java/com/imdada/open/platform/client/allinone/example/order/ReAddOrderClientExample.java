package com.imdada.open.platform.client.allinone.example.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.annotation.JSONField;
import com.github.kevinsawicki.http.HttpRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 重新发布订单代码示例 <br/>
 * 需要引入以下依赖(jdk8及以上)
 * <pre> {@code
 *    <dependency>
 *        <groupId>org.apache.commons</groupId>
 *        <artifactId>commons-lang3</artifactId>
 *        <version>3.4</version>
 *    </dependency>
 *    <dependency>
 *        <groupId>commons-codec</groupId>
 *        <artifactId>commons-codec</artifactId>
 *        <version>1.11</version>
 *    </dependency>
 *    <dependency>
 *        <groupId>ch.qos.logback</groupId>
 *        <artifactId>logback-classic</artifactId>
 *        <version>1.2.3</version>
 *    </dependency>
 *    <dependency>
 *        <groupId>org.projectlombok</groupId>
 *        <artifactId>lombok</artifactId>
 *        <version>1.16.16</version>
 *    </dependency>
 *    <dependency>
 *        <groupId>com.alibaba</groupId>
 *        <artifactId>fastjson</artifactId>
 *        <version>1.2.58</version>
 *    </dependency>
 *    <dependency>
 *        <groupId>com.github.kevinsawicki</groupId>
 *        <artifactId>http-request</artifactId>
 *        <version>6.0</version>
 *    </dependency>
 *  }</pre>
 */
@Slf4j
public class ReAddOrderClientExample {

    /**
     * 重新发布订单接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/reAddOrder";

    public static void main(String[] args) {
        new ReAddOrderClientExample().execute();
    }

    /**
     * 重新发布订单(除了url以及订单状态有要求之外，其与逻辑和新增订单/api/order/addOrder的流程一致)
     */
    private void execute() {
        // 这里需要是一个【已经下发过的】，当前是【已取消】或者【返还完成】状态的订单单号
        // 否则会提示【订单未终结(已取消、已过期、投递异常的订单才能重发)】
        String originId = "originId-1637910090457";

        // 构建基本信息
        OrderDetail body = buildBody(originId);

        // 构建真正的api接口参数
        Map<String, Object> params = generateParams(body);

        try {
            // 执行请求
            String rpcResult = doInvoke(URL, params);

            // 处理结果
            processResponse(rpcResult);
        } catch (Exception e) {
            // 异常后的处理逻辑，可能由于网络或其他因素导致失败，建议添加重试
            log.error("执行异常，参数：{}", params, e);
        }
    }

    /**
     * 处理结果
     */
    private void processResponse(String rpcResult) {
        // 根据status判断是否执行成功
        DaDaResponse<?> response = JSON.parseObject(rpcResult, new TypeReference<DaDaResponse<?>>() {
        });
        if (Objects.nonNull(response) && Objects.equals(response.getStatus(), "success")) {
            log.info("执行成功，响应结果：{}", JSON.toJSONString(response.getResult()));
        } else {
            log.info("执行失败，响应结果：{}", rpcResult);
        }
    }

    /**
     * 执行http请求(可以根据实际情况替换为自己的HTTP请求方式)
     *
     * @param url    请求url
     * @param params 请求参数对象
     * @return 响应结果
     */
    private String doInvoke(String url, Map<String, Object> params) {
        log.info("开始执行请求, url: {}, 参数: {}", url, JSON.toJSONString(params));
        return HttpRequest.post(url)
                .header("content-type", "application/json")
                .header("accept", "application/json")
                .connectTimeout(1000)
                .readTimeout(5000)
                .send(JSON.toJSONString(params))
                .body();
    }

    /**
     * 构建真正的api接口参数
     *
     * @param body 接口的body参数内容
     * @return 构建真正的api接口参数
     */
    private Map<String, Object> generateParams(Object body) {
        String appSecret = Configuration.getInstance().getAppSecret();
        Map<String, Object> data = new HashMap<>();
        data.put("source_id", Configuration.getInstance().getSourceId());
        data.put("app_key", Configuration.getInstance().getAppKey());
        data.put("timestamp", LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond());
        data.put("format", "json");
        data.put("v", "1.0");
        data.put("body", JSON.toJSONString(body));
        data.put("signature", signature(data, appSecret));
        return data;
    }

    /**
     * 生成签名(具体参考文档: http://newopen.imdada.cn/#/quickStart/develop/safety?_k=kklqac)
     */
    private String signature(Map<String, Object> data, String appSecret) {
        // 请求参数按照【属性名】字典升序排序后，按照属性名+属性值拼接
        String signStr = data.keySet().stream()
                .sorted()
                .map(it -> String.format("%s%s", it, data.get(it)))
                .collect(Collectors.joining(""));

        // 拼接后的结果首尾加上appSecret
        String finalSignStr = appSecret + signStr + appSecret;

        // MD5加密并转为大写
        return DigestUtils.md5Hex(finalSignStr).toUpperCase();
    }

    /**
     * 构建发单参数(请根据自己实际情况调整参数)
     */
    private static OrderDetail buildBody(String orderNo) {
        ProductDetail productDetail = ProductDetail.builder()
                .skuName("高原风干牛肉干（麻辣味）250克/袋 | 250克")
                .count(2.0)
                .srcProductNo("xxx")
                .unit("件")
                .build();

        return OrderDetail.builder()
                .shopNo(Configuration.getInstance().getShopNo())
                .originId(orderNo)
                .cityCode("021")
                .cargoPrice(50.0)
                .cargoNum(1)
                .tips(1.0)
                .info("备注信息")
                .cargoWeight(3.2)
                .isUseInsurance(0)
                .prepay(0)
                .finishCodeNeeded(0)
                .cargoType(50)
                .invoiceTitle("invoiceTitle")
                .receiverName("张三")
                .receiverAddress("上海市浦东新区东方渔人码头")
                .receiverPhone("18500000000")
                .receiverLat(31.257801)
                .receiverLng(121.538842)
                .callback(Configuration.getInstance().getCallback())
                .pickUpPos("1号货架")
                .productList(Collections.singletonList(productDetail))
                .build();
    }

    /**
     * 达达开放平台api接口-环境相关配置(请根据自己实际情况调整参数)
     */
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Configuration {

        /**
         * 达达开放平台域名(qa环境:newopen.qa.imdada.cn; 生产环境: newopen.imdada.cn, http和https都支持)
         */
        private String host;

        /**
         * 发单回调地址(接单、到店、取货、送达等消息通知，达达会通过该接口通知到商户)
         */
        private String callback;

        /**
         * 大客户id-即商户编号(账户中心-基本资料-商户编号)
         */
        private String sourceId;

        /**
         * 门店编号(门店标识，用商户账号在达达开放平台登录后，在管理中心-商户中心-门店管理中可以查看编号)
         * 实际情况下会有多个门店，这里为了方便测试，固定配置一个门店
         */
        private String shopNo;

        /**
         * 开发者登录后，在管理中心-开发助手-应用信息页面可以看到
         */
        private String appKey;

        /**
         * 开发者登录后，在管理中心-开发助手-应用信息页面可以看到
         */
        private String appSecret;

        /**
         * 请根据实际情况配置以下参数
         */
        private static Configuration INSTANCE = Configuration.builder()
                // 达达开放平台接口host(生成环境请修改为: newopen.imdada.cn)
                .host("http://newopen.qa.imdada.cn")
                // 请配置为自己的回调地址
                .callback("http://baidu.com")
                // 请配置为自己的大客户id
                .sourceId("73753")
                // 请配置为自己的门店编号
                .shopNo("11047059")
                // 请配置为自己的appKey
                .appKey("dadad0119a3f3805d67")
                // 请配置为自己的appSecret
                .appSecret("f3a9845ffbbd5b6ec5d489dfb5555846")
                .build();

        public static Configuration getInstance() {
            return INSTANCE;
        }

    }

    /**
     * 订单信息
     */
    @Data
    @Builder
    public static class OrderDetail {

        @JSONField(name = "shop_no")
        private String shopNo;

        @JSONField(name = "origin_id")
        private String originId;

        @JSONField(name = "city_code")
        private String cityCode;

        @JSONField(name = "cargo_price")
        private Double cargoPrice;

        @JSONField(name = "is_prepay")
        private Integer prepay;

        @JSONField(name = "receiver_name")
        private String receiverName;

        @JSONField(name = "receiver_address")
        private String receiverAddress;

        @JSONField(name = "receiver_lat")
        private Double receiverLat;

        @JSONField(name = "receiver_lng")
        private Double receiverLng;

        private String callback;

        @JSONField(name = "cargo_weight")
        private Double cargoWeight;

        @JSONField(name = "receiver_phone")
        private String receiverPhone;

        @JSONField(name = "receiver_tel")
        private String receiverTel;

        @JSONField(name = "tips")
        private Double tips;

        @JSONField(name = "info")
        private String info = "";

        @JSONField(name = "cargo_type")
        private Integer cargoType;

        @JSONField(name = "cargo_num")
        private Integer cargoNum;

        @JSONField(name = "invoice_title")
        private String invoiceTitle;

        @JSONField(name = "origin_mark")
        private String originMark;

        @JSONField(name = "origin_mark_no")
        private String originMarkNo;

        @JSONField(name = "is_use_insurance")
        private Integer isUseInsurance;

        @JSONField(name = "is_finish_code_needed")
        private Integer finishCodeNeeded;

        @JSONField(name = "delay_publish_time")
        private Integer delayPublishTime;

        @JSONField(name = "is_direct_delivery")
        private Integer isDirectDelivery;

        @JSONField(name = "pick_up_pos")
        private String pickUpPos;

        @JSONField(name = "product_list")
        private List<ProductDetail> productList;

    }

    @Data
    @Builder
    public static class ProductDetail {

        @JSONField(name = "src_product_no")
        private String srcProductNo;

        @JSONField(name = "sku_name")
        private String skuName;

        @JSONField(name = "count")
        private Double count;

        @JSONField(name = "unit")
        private String unit;

    }

    /**
     * 接口返回结果对象
     */
    @Data
    public static class DaDaResponse<T> {

        private String status;
        private T result;
        private Integer code;
        private String msg;

    }

}
