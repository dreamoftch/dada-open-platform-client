package com.imdada.open.platform.util;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 三方单号生成器(请根据自己的场景替换为自己的单号生成器)
 */
@Slf4j
public class OrderNoGenerator {

    private static final AtomicLong LONG_ADDER = new AtomicLong(System.currentTimeMillis());

    /**
     * 生成三方单号
     *
     * @return
     */
    public static String generateOrderNo() {
        long suffix = LONG_ADDER.incrementAndGet();
        String orderNo = String.format("orderNo%s", suffix);
        log.info("生成单号：{}", orderNo);
        return orderNo;
    }

}
