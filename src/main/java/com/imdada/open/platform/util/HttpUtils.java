package com.imdada.open.platform.util;

import com.alibaba.fastjson.JSON;
import com.github.kevinsawicki.http.HttpRequest;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public final class HttpUtils {

    /**
     * 执行http post请求(可以根据实际情况替换为自己的HTTP请求方式)
     *
     * @param url    请求url
     * @param params 请求参数对象
     * @return 响应结果
     */
    public static String doPostJson(String url, Map<String, Object> params) {
        try {
            String response = HttpRequest.post(url)
                    .header("content-type", "application/json")
                    .header("accept", "application/json")
                    .connectTimeout(2000)
                    .readTimeout(5000)
                    .send(JSON.toJSONString(params))
                    .body();
            log.info("rpc执行完成, url: {}, 参数: {}, response: {}", url, JSON.toJSONString(params), response);
            return response;
        } catch (Exception e) {
            log.error("rpc执行异常, url: {}, 参数: {}", url, params, e);
            throw e;
        }
    }

}
