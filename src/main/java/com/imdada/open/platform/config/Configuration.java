package com.imdada.open.platform.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 调用达达开放平台api接口-环境相关配置(请根据自己实际情况调整参数)
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Configuration {

    /**
     * 达达开放平台域名(qa环境:newopen.qa.imdada.cn; 生产环境: newopen.imdada.cn, http和https都支持)
     */
    private String host;

    /**
     * 发单回调地址(接单、到店、取货、送达等消息通知，达达会通过该接口通知到商户)
     */
    private String callback;

    /**
     * 大客户id-即商户编号(账户中心-基本资料-商户编号)
     */
    private String sourceId;

    /**
     * 门店编号(门店标识，用商户账号在达达开放平台登录后，在管理中心-商户中心-门店管理中可以查看编号)
     * 实际情况下会有多个门店，这里为了方便测试，固定配置一个门店
     */
    private String shopNo;

    /**
     * 开发者登录后，在管理中心-开发助手-应用信息页面可以看到
     */
    private String appKey;

    /**
     * 开发者登录后，在管理中心-开发助手-应用信息页面可以看到
     */
    private String appSecret;

    /**
     * 请根据实际情况配置以下参数
     */
    private static Configuration INSTANCE = Configuration.builder()
            // 达达开放平台接口host(生成环境请修改为: newopen.imdada.cn)
            .host("http://newopen.qa.imdada.cn")
            // 请配置为自己的回调地址
            .callback("http://baidu.com")
            // 请配置为自己的大客户id
            .sourceId("73753")
            // 请配置为自己的门店编号
            .shopNo("11047059")
            // 请配置为自己的appKey
            .appKey("dadad0119a3f3805d67")
            // 请配置为自己的appSecret
            .appSecret("f3a9845ffbbd5b6ec5d489dfb5555846")
            .build();

    public static Configuration getInstance() {
        return INSTANCE;
    }

}
