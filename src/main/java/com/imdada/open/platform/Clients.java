package com.imdada.open.platform;

import com.imdada.open.platform.client.message.MessageConfirmClient;
import com.imdada.open.platform.exception.RpcException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Clients {

    public static void main(String[] args) throws RpcException {
//        AddOrderResp addOrderResp = AddOrderClient.execute();
//        CancelOrderClient.execute("orderNo1653708880866");
//        QueryDeliverFeeAndAddOrderClient.execute("orderNo1653708880866");
//        ReAddOrderClient.execute("orderNo1653708880866");
//        QueryOrderStatusClient.execute("orderNo1653708880866");
//        AddShopClient.execute();
//        UpdateShopClient.execute();
//        QueryShopDetailClient.execute();
//        QueryCityListClient.execute();
//        AppointOrderClient.execute("xxx", 1L);
//        CancelAppointOrderClient.execute("xxx");
//        ConfirmGoodsClient.execute("xxx");
//        QueryCancelReasonsClient.execute();
//        AddTipClient.execute("xxx");
//        ComplaintTransporterClient.execute("xxx");
//        QueryAppointTransportersClient.execute();
//        QueryBalanceClient.execute();
//        GenerateRechargeUrlClient.execute("H5", BigDecimal.valueOf(0.1), null);
        MessageConfirmClient.execute("xxx", 1);
    }

}
