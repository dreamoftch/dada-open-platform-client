package com.imdada.open.platform.exception;

public class RpcException extends Exception {

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcException(String message) {
        super(message);
    }

}
