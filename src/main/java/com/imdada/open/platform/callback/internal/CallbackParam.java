package com.imdada.open.platform.callback.internal;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class CallbackParam {

    @JSONField(name = "order_id")
    private String orderId;

    @JSONField(name = "client_id")
    private String clientId;

    @JSONField(name = "order_status")
    private Integer orderStatus;

    @JSONField(name = "cancel_reason")
    private String cancelReason;

    @JSONField(name = "cancel_from")
    private Integer cancelFrom;

    @JSONField(name = "update_time")
    private Long updateTime;

    private String signature;

    @JSONField(name = "dm_id")
    private long dmId;

    @JSONField(name = "dm_name")
    private String dmName;

    @JSONField(name = "dm_mobile")
    private String dmMobile;

    @JSONField(name = "finish_code")
    private String finishCode;

}