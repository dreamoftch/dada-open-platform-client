package com.imdada.open.platform.callback.internal;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 达达回调状态枚举类
 */
@Getter
@AllArgsConstructor
public enum DaDaCallbackStatusEnum {

    WAIT_ACCEPT(1, "订单已创建成功-待接单"),
    WAIT_PICK(2, "已接单-待取货"),
    ARRIVE_SHOP(100, "已取货-待到店"),
    DELIVERING(3, "已取货-配送中"),
    HAD_COMPLETE(4, "已完成"),
    HAD_CANCEL(5, "已取消"),
    RETURN_ING(9, "妥投异常,返还中"),
    ORDER_CREATE_FAILED(1000, "系统异常,订单下发失败"),
    RETURN_SURE(10, "妥投异常,返还完成"),
    RETURN_SHOP(11, "返还到店"),
    ;

    private int orderStatusCode;

    private String desc;

}
