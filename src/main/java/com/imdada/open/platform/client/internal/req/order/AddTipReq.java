package com.imdada.open.platform.client.internal.req.order;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddTipReq {

    @JSONField(name = "order_id")
    private String orderId;

    private Float tips;

    @JSONField(name = "city_code")
    private String cityCode;

    private String info;

}