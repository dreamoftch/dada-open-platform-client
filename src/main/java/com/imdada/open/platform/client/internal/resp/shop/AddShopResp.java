package com.imdada.open.platform.client.internal.resp.shop;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AddShopResp {
    private Integer success;
    private List<AddShopSuccessResp> successList;
    private List<AddShopFailedResp> failedList;

    @Data
    public static class AddShopFailedResp {

        private String shopNo;
        private String msg;
        private String shopName;
        private Integer code;

    }

    @Data
    public static class AddShopSuccessResp {

        @JSONField(name = "station_name")
        private String stationName;

        @JSONField(name = "origin_shop_id")
        private String originShopId;

        @JSONField(name = "contact_name")
        private String contactName;

        private String phone;

        @JSONField(name = "station_address")
        private String stationAddress;

        private Integer business;

        private Double lng;

        private Double lat;

        @JSONField(name = "id_card")
        private String legalId;

        private String username;

        private String password;

        @JSONField(name = "label_name")
        private String labelName;

        /**
         * 结算方式，0-跟随大客户结算，1-独立结算
         */
        @JSONField(name = "settlement_type")
        private Integer settlementType;

    }

}