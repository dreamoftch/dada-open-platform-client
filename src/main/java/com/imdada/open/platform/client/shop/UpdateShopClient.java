package com.imdada.open.platform.client.shop;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.shop.UpdateShopReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 编辑门店代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/shopUpdate)
 */
@Slf4j
public class UpdateShopClient {

    /**
     * 编辑门店接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/shop/update";

    public static void execute() throws RpcException {
        // 构建基本信息
        UpdateShopReq body = buildBody();

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    private static UpdateShopReq buildBody() {
        return UpdateShopReq.builder()
                .stationName("杨浦区肉夹馍")
                .business(1)
                .stationAddress("上海市杨浦区东方渔人码头")
                .lng(121.538842)
                .lat(31.257801)
                .contactName("李四")
                .phone("18500000012")
                .originShopId("700002")
                .build();
    }

}
