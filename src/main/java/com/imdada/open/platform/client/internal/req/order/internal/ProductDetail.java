package com.imdada.open.platform.client.internal.req.order.internal;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDetail {

    @JSONField(name = "src_product_no")
    private String srcProductNo;

    @JSONField(name = "sku_name")
    private String skuName;

    @JSONField(name = "count")
    private Double count;

    @JSONField(name = "unit")
    private String unit;

}
