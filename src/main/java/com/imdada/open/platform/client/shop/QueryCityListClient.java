package com.imdada.open.platform.client.shop;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 获取城市信息代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/cityList)
 */
@Slf4j
public class QueryCityListClient {

    /**
     * 获取城市信息接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/cityCode/list";

    public static void execute() throws RpcException {
        // 构建基本信息(当没有业务参数的时候, body需要赋值为空字符串,即body:"")
        String body = "";

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

}
