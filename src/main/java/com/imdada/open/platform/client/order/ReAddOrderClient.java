package com.imdada.open.platform.client.order;

import com.imdada.open.platform.client.internal.resp.order.AddOrderResp;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.order.AddOrderReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import com.imdada.open.platform.util.AddOrderUtils;
import com.imdada.open.platform.util.OrderNoGenerator;
import lombok.extern.slf4j.Slf4j;

/**
 * 重新发布订单代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/reAdd)
 */
@Slf4j
public class ReAddOrderClient {

    /**
     * 重新发布订单接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/reAddOrder";

    public static AddOrderResp execute() throws RpcException {
        // 生成一个三方单号
        String orderNo = OrderNoGenerator.generateOrderNo();
        return execute(orderNo);
    }

    /**
     * 重新发布订单(除了url以及订单状态有要求之外，其与逻辑和新增订单/api/order/addOrder的流程一致)
     */
    public static AddOrderResp execute(String orderNo) throws RpcException {
        // 这里需要是一个【已经下发过的】，当前是【已取消】或者【返还完成】状态的订单单号
        // 否则会提示【订单未终结(已取消、已过期、投递异常的订单才能重发)】

        // 构建基本信息
        AddOrderReq body = AddOrderUtils.buildOrderDetail4Demo(orderNo);

        // 根据业务参数，调用达达api接口，并处理response
        String response = ApiUtils.callApiWithBizParam(URL, body);

        return AddOrderUtils.parseAddOrderResponse(response, "调用达达重发单接口失败");
    }

}
