package com.imdada.open.platform.client.order;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.order.CancelOrderReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 取消订单代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/formalCancel)
 */
@Slf4j
public class CancelOrderClient {

    /**
     * 取消订单接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/formalCancel";

    public static void execute(String orderNo) throws RpcException {
        // 构建基本信息
        CancelOrderReq body = buildBody(orderNo);

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    /**
     * 构建参数(请根据自己实际情况调整参数)
     */
    private static CancelOrderReq buildBody(String orderNo) {
        return CancelOrderReq.builder()
                .orderId(orderNo)
                .cancelReasonId(1)
                .cancelReason("没有配送员接单")
                .build();
    }

}
