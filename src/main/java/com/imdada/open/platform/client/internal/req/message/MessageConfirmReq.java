package com.imdada.open.platform.client.internal.req.message;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MessageConfirmReq {

    private Integer messageType;

    private String messageBody;

}
