package com.imdada.open.platform.client.internal.req.order;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AppointOrderReq {

    @JSONField(name = "order_id")
    private String orderId;

    @JSONField(name = "transporter_id")
    private Long transporterId;

    @JSONField(name = "shop_no")
    private String shopNo;

}