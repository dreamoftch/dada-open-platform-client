package com.imdada.open.platform.client.internal.req.shop;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddShopReq {

    @JSONField(name = "station_name")
    private String stationName;

    private Integer business;

    @JSONField(name = "station_address")
    private String stationAddress;

    private Double lng;

    private Double lat;

    @JSONField(name = "contact_name")
    private String contactName;

    private String phone;

    @JSONField(name = "origin_shop_id")
    private String originShopId;

    @JSONField(name = "id_card")
    private String idCard;

    private String username;

    private String password;

}