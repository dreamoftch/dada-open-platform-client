package com.imdada.open.platform.client.internal.req.recharge;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class GenerateRechargeUrlReq {

    private String category;
    private BigDecimal amount;
    @JSONField(name = "notify_url")
    private String notifyUrl;

}
