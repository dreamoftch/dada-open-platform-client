package com.imdada.open.platform.client.internal.req.shop;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddMerchantReq {

    private String mobile;

    @JSONField(name = "contact_name")
    private String contactName;

    @JSONField(name = "contact_phone")
    private String contactPhone;

    @JSONField(name = "enterprise_name")
    private String enterpriseName;

    @JSONField(name = "city_name")
    private String cityName;

    @JSONField(name = "enterprise_address")
    private String enterpriseAddress;

    private String email;

    private String bdCode;

}