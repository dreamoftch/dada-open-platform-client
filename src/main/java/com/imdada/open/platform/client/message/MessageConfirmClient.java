package com.imdada.open.platform.client.message;

import com.alibaba.fastjson.JSON;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.message.MessageConfirmReq;
import com.imdada.open.platform.client.internal.req.message.TransporterCancelOrderConfirmReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 消息确认(接口文档地址：http://newopen.imdada.cn/#/development/file/merchantConfirm)
 */
@Slf4j
public class MessageConfirmClient {

    /**
     * 消息确认接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/message/confirm";

    public static void execute(String orderNo, Integer confirmResult) throws RpcException {
        // 构建基本信息
        MessageConfirmReq body = buildBody(orderNo, confirmResult);

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    /**
     * 构建参数(请根据自己实际情况调整参数)
     */
    private static MessageConfirmReq buildBody(String orderNo, Integer confirmResult) {
        // 同意骑士取消的示例
        TransporterCancelOrderConfirmReq messageBody = TransporterCancelOrderConfirmReq.builder()
                .orderId(orderNo)
                // isConfirm = 1 表示同意
                .isConfirm(confirmResult)
                .build();
        return MessageConfirmReq.builder()
                // messageType = 1 表示【骑士取消订单推送消息】
                .messageType(1)
                .messageBody(JSON.toJSONString(messageBody))
                .build();
    }

}
