package com.imdada.open.platform.client.shop;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.shop.AddMerchantReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 注册商户代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/merchantAdd)
 */
@Slf4j
public class AddMerchantClient {

    /**
     * 注册商户接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/merchantApi/merchant/add";

    public static void execute() throws RpcException {
        // 构建基本信息
        AddMerchantReq body = buildBody();

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    private static AddMerchantReq buildBody() {
        return AddMerchantReq.builder()
                .mobile("18500000000")
                .cityName("上海")
                .enterpriseName("测试商户")
                .enterpriseAddress("上海市杨浦区东方渔人码头")
                .contactName("张三")
                .contactPhone("18500000000")
                .email("zhangsan@xxx.com")
                .build();
    }

}
