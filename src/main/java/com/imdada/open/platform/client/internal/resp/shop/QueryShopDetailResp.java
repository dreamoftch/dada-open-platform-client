package com.imdada.open.platform.client.internal.resp.shop;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class QueryShopDetailResp {

	@JSONField(name = "station_name")
	private String stationName;

	@JSONField(name = "origin_shop_id")
	private String originShopId;

	@JSONField(name = "contact_name")
	private String contactName;

	private String phone;

	private Integer status;

	@JSONField(name = "station_address")
	private String stationAddress;

	private Integer business;

	private Double lng;

	private Double lat;

	@JSONField(name = "label_name")
	private String labelName;

	@JSONField(name = "city_name")
	private String cityName;

	@JSONField(name = "area_name")
	private String areaName;

	/**
	 * 结算方式，0-跟随大客户结算，1-独立结算
	 */
	@JSONField(name = "settlement_type")
	private Integer settlementType;

}