package com.imdada.open.platform.client.order;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.order.AddTipReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;


/**
 * 增加小费代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/addTip)
 */
@Slf4j
public class AddTipClient {

    /**
     * 增加小费接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/addTip";

    public static void execute(String orderNo) throws RpcException {
        // 构建基本信息
        AddTipReq body = buildBody(orderNo);

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    /**
     * 构建参数(请根据自己实际情况调整参数)
     */
    private static AddTipReq buildBody(String orderNo) {
        return AddTipReq.builder()
                .orderId(orderNo)
                .tips(3.0f)
                .cityCode("021")
                .info("加个小费")
                .build();
    }

}
