package com.imdada.open.platform.client.internal.resp.recharge;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class QueryBalanceResp {

    private BigDecimal deliverBalance;
    private BigDecimal redPacketBalance;

}
