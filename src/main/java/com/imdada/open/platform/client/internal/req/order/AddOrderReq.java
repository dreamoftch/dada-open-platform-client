package com.imdada.open.platform.client.internal.req.order;

import com.alibaba.fastjson.annotation.JSONField;
import com.imdada.open.platform.client.internal.req.order.internal.ProductDetail;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AddOrderReq {

    @JSONField(name = "shop_no")
    private String shopNo;

    @JSONField(name = "origin_id")
    private String originId;

    @JSONField(name = "city_code")
    private String cityCode;

    @JSONField(name = "cargo_price")
    private Double cargoPrice;

    @JSONField(name = "is_prepay")
    private Integer prepay;

    @JSONField(name = "receiver_name")
    private String receiverName;

    @JSONField(name = "receiver_address")
    private String receiverAddress;

    @JSONField(name = "receiver_lat")
    private Double receiverLat;

    @JSONField(name = "receiver_lng")
    private Double receiverLng;

    private String callback;

    @JSONField(name = "cargo_weight")
    private Double cargoWeight;

    @JSONField(name = "receiver_phone")
    private String receiverPhone;

    @JSONField(name = "receiver_tel")
    private String receiverTel;

    @JSONField(name = "tips")
    private Double tips;

    @JSONField(name = "info")
    private String info = "";

    @JSONField(name = "cargo_type")
    private Integer cargoType;

    @JSONField(name = "cargo_num")
    private Integer cargoNum;

    @JSONField(name = "invoice_title")
    private String invoiceTitle;

    @JSONField(name = "origin_mark")
    private String originMark;

    @JSONField(name = "origin_mark_no")
    private String originMarkNo;

    @JSONField(name = "is_use_insurance")
    private Integer isUseInsurance;

    @JSONField(name = "is_finish_code_needed")
    private Integer finishCodeNeeded;

    @JSONField(name = "delay_publish_time")
    private Integer delayPublishTime;

    @JSONField(name = "is_direct_delivery")
    private Integer isDirectDelivery;

    @JSONField(name = "pick_up_pos")
    private String pickUpPos;

    @JSONField(name = "product_list")
    private List<ProductDetail> productList;

}
