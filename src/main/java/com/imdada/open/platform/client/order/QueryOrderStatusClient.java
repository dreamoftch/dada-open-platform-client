package com.imdada.open.platform.client.order;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;

/**
 * 订单详情查询代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/statusQuery)
 */
@Slf4j
public class QueryOrderStatusClient {

    /**
     * 订单详情查询接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/status/query";

    public static void execute(String orderNo) throws RpcException {
        // 构建基本信息
        Map<String, String> body = Collections.singletonMap("order_id", orderNo);

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

}
