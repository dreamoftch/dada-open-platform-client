package com.imdada.open.platform.client.recharge;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.imdada.open.platform.client.internal.req.common.ApiResponse;
import com.imdada.open.platform.client.internal.req.recharge.GenerateRechargeUrlReq;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * 生成充值的链接(接口文档地址：http://newopen.imdada.cn/#/development/file/recharge)
 */
@Slf4j
public class GenerateRechargeUrlClient {

    /**
     * 消息确认接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/recharge";

    public static void execute(String category, BigDecimal amount, String notifyUrl) {
        // 构建基本信息
        GenerateRechargeUrlReq body = buildBody(category, amount, notifyUrl);

        // 根据业务参数，调用达达api接口，并处理response
        String response = ApiUtils.callApiWithBizParam(URL, body);

        ApiResponse<String> result = JSON.parseObject(response, new TypeReference<ApiResponse<String>>() {
        });

        log.info("充值链接: {}", JSON.toJSONString(result.getResult()));
    }

    /**
     * 修改收货人信息参数
     */
    private static GenerateRechargeUrlReq buildBody(String category, BigDecimal amount, String notifyUrl) {
        return GenerateRechargeUrlReq.builder()
                .category(category)
                .amount(amount)
                .notifyUrl(notifyUrl)
                .build();
    }

}
