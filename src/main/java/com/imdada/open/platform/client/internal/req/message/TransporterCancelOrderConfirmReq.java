package com.imdada.open.platform.client.internal.req.message;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TransporterCancelOrderConfirmReq {

    private String orderId;

    private Long dadaOrderId;

    private Integer isConfirm;

    private List<String> imgs;

    private String rejectReason;

}
