package com.imdada.open.platform.client.internal.req.order;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CancelOrderReq {

    @JSONField(name = "order_id")
    private String orderId;

    @JSONField(name = "cancel_reason_id")
    private Integer cancelReasonId;

    @JSONField(name = "cancel_reason")
    private String cancelReason;

}