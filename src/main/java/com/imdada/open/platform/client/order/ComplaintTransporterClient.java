package com.imdada.open.platform.client.order;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.order.ComplaintTransporterReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 商家投诉达达代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/complaintDada)
 */
@Slf4j
public class ComplaintTransporterClient {

    /**
     * 商家投诉达达接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/complaint/dada";

    public static void execute(String orderNo) throws RpcException {
        // 构建基本信息
        ComplaintTransporterReq body = buildBody(orderNo);

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    /**
     * 构建参数(请根据自己实际情况调整参数)
     */
    private static ComplaintTransporterReq buildBody(String orderNo) {
        return ComplaintTransporterReq.builder()
                .orderId(orderNo)
                .reasonId(1)
                .build();
    }

}
