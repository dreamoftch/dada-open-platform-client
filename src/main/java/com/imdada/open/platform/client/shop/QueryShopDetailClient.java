package com.imdada.open.platform.client.shop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.imdada.open.platform.client.internal.req.common.ApiResponse;
import com.imdada.open.platform.client.internal.resp.shop.QueryShopDetailResp;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;

/**
 * 门店详情代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/shopDetail)
 */
@Slf4j
public class QueryShopDetailClient {

    /**
     * 门店详情接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/shop/detail";

    public static void execute() {
        // 构建基本信息
        Map<String, String> body = Collections.singletonMap("origin_shop_id", "700002");

        // 根据业务参数，调用达达api接口，并处理response
        String response = ApiUtils.callApiWithBizParam(URL, body);

        ApiResponse<QueryShopDetailResp> result = JSON.parseObject(response, new TypeReference<ApiResponse<QueryShopDetailResp>>() {
        });
        log.info("门店详情为: {}", JSON.toJSONString(result.getResult()));
    }

}
