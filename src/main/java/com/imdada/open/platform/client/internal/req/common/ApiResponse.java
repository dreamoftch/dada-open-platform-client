package com.imdada.open.platform.client.internal.req.common;

import lombok.Data;

import java.util.Objects;

/**
 * 达达api接口返回结果对象
 */
@Data
public class ApiResponse<T> {

    private String status;
    private T result;
    private Integer code;
    private String msg;

    /**
     * 判断rpc请求是否成功
     * @return
     */
    public boolean success() {
        return Objects.equals(status, "success");
    }

}
