package com.imdada.open.platform.client.recharge;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.imdada.open.platform.client.internal.req.common.ApiResponse;
import com.imdada.open.platform.client.internal.resp.recharge.QueryBalanceResp;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;

/**
 * 查询账户余额(接口文档地址：http://newopen.imdada.cn/#/development/file/balanceQuery)
 */
@Slf4j
public class QueryBalanceClient {

    /**
     * 消息确认接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/balance/query";

    public static void execute() {
        // 构建基本信息(category: 查询运费账户类型 1：运费账户；2：红包账户，3：所有，默认查询运费账户余额)
        Map<String, Integer> body = Collections.singletonMap("category", 3);

        // 根据业务参数，调用达达api接口，并处理response
        String response = ApiUtils.callApiWithBizParam(URL, body);

        ApiResponse<QueryBalanceResp> result = JSON.parseObject(response, new TypeReference<ApiResponse<QueryBalanceResp>>() {
        });

        log.info("账户余额: {}", JSON.toJSONString(result.getResult()));
    }

}
