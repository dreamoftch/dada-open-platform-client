package com.imdada.open.platform.client.internal.resp.order;

import lombok.Data;

@Data
public class AddOrderResp {

    /**
     * 配送距离(单位：米)
     */
    private Double distance;

    /**
     * 实际运费(单位：元)，运费减去优惠券费用
     */
    private Double fee;

    /**
     * 运费(单位：元)
     */
    private Double deliverFee;

    /**
     * 优惠券费用(单位：元)
     */
    private Double couponFee;

    /**
     * 保价费(单位：元)
     */
    private Double insuranceFee;

    /**
     * 小费(单位：元)
     */
    private Double tips;

    /**
     * 运单号(查询运费接口会返回该字段，用于查询运费后发单使用)
     */
    private String deliveryNo;

}