package com.imdada.open.platform.client.order;

import com.imdada.open.platform.client.internal.req.order.AddOrderReq;
import com.imdada.open.platform.client.internal.resp.order.AddOrderResp;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.AddOrderUtils;
import com.imdada.open.platform.util.ApiUtils;
import com.imdada.open.platform.util.OrderNoGenerator;
import lombok.extern.slf4j.Slf4j;

/**
 * 新增订单代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/add)
 */
@Slf4j
public class AddOrderClient {

    /**
     * 发单接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/addOrder";

    public static AddOrderResp execute() throws RpcException {
        // 生成一个三方单号
        String orderNo = OrderNoGenerator.generateOrderNo();
        return execute(orderNo);
    }

    public static AddOrderResp execute(String orderNo) throws RpcException {
        // 构建基本信息
        AddOrderReq body = AddOrderUtils.buildOrderDetail4Demo(orderNo);

        // 根据业务参数，调用达达api接口，并处理response
        String response = ApiUtils.callApiWithBizParam(URL, body);

        return AddOrderUtils.parseAddOrderResponse(response, "调用达达发单接口失败");
    }

}
