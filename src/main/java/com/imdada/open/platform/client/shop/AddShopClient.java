package com.imdada.open.platform.client.shop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.imdada.open.platform.client.internal.req.common.ApiResponse;
import com.imdada.open.platform.client.internal.req.shop.AddShopReq;
import com.imdada.open.platform.client.internal.resp.shop.AddShopResp;
import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;

/**
 * 新增门店代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/shopAdd)
 */
@Slf4j
public class AddShopClient {

    /**
     * 新增门店接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/shop/add";

    public static void execute() {
        // 构建基本信息(请注意：这里body是个list或者数组，而不是单个对象!!!!!!)
        List<AddShopReq> body = buildBody();

        // 根据业务参数，调用达达api接口，并处理response
        String response = ApiUtils.callApiWithBizParam(URL, body);

        ApiResponse<AddShopResp> result = JSON.parseObject(response, new TypeReference<ApiResponse<AddShopResp>>() {
        });

        log.info("创建成功的门店: {}", JSON.toJSONString(result.getResult().getSuccessList()));
        log.info("创建失败的门店: {}", JSON.toJSONString(result.getResult().getFailedList()));
    }

    private static List<AddShopReq> buildBody() {
        AddShopReq addShopReq = AddShopReq.builder()
                .stationName("杨浦区肉夹馍3号")
                .business(1)
                .stationAddress("上海市杨浦区东方渔人码头3号")
                .lng(121.538842)
                .lat(31.257801)
                .contactName("李四")
                .phone("18500000003")
                .originShopId("700003")
                .build();
        return Collections.singletonList(addShopReq);
    }

}
