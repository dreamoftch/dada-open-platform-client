package com.imdada.open.platform.client.internal.req.order;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ComplaintTransporterReq {

    @JSONField(name = "order_id")
    private String orderId;

    @JSONField(name = "reason_id")
    private Integer reasonId;

}