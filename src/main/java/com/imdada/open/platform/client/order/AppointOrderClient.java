package com.imdada.open.platform.client.order;

import com.imdada.open.platform.config.Configuration;
import com.imdada.open.platform.client.internal.req.order.AppointOrderReq;
import com.imdada.open.platform.exception.RpcException;
import com.imdada.open.platform.util.ApiUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 追加订单代码示例(接口文档地址：http://newopen.imdada.cn/#/development/file/appointOrder)
 */
@Slf4j
public class AppointOrderClient {

    /**
     * 追加订单接口URL
     */
    private static final String URL = Configuration.getInstance().getHost() + "/api/order/appoint/exist";

    public static void execute(String orderNo, Long transporterId) throws RpcException {
        // 构建基本信息
        AppointOrderReq body = buildBody(orderNo, transporterId);

        // 根据业务参数，调用达达api接口，并处理response
        ApiUtils.callApiWithBizParamAndLogResponse(URL, body);
    }

    /**
     * 构建参数(请根据自己实际情况调整参数)
     */
    private static AppointOrderReq buildBody(String orderNo, Long transporterId) {
        return AppointOrderReq.builder()
                .orderId(orderNo)
                .transporterId(transporterId)
                .shopNo(Configuration.getInstance().getShopNo())
                .build();
    }

}
