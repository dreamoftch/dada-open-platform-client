# dada-open-platform-client

#### 项目介绍
该项目为[达达开放平台](http://newopen.imdada.cn/)对接client代码示例

#### 为什么会有这个项目
目前每个商户对接达达的大致流程：
- 对接开发者：阅读达达开放平台网站文档
- 对接开发者：根据文档开发代码
- 对接开发者：联调代码并发现问题
- 对接开发者：在对接群反馈问题
- 达达技术支持或开发：同学解答
- 对接开发者：修改代码
- 对接开发者：重新联调

随着达达开放平台对接商户越来越多，每个商户都需要从头开始经历这些步骤，对外部开发者、达达侧开发同学都是困扰。

所以希望通过这个项目，最大程度的降低外部开发者的开发时间，而且该项目代码是经过验证的，可以最大程度减少代码对接问题。

#### 快速开始
为便于开发者对达达相关接口有个快速的了解，
可以直接运行 `com.imdada.open.platform.Clients` 的main方法，
实现在达达QA环境发单

> 需要注意的是达达发单的内部逻辑是异步的，<br/>
> 所以在发单接口返回成功后，立即查询订单详情的时候，<br/>
> 可能提示订单不存在，<br/>
> 建议在达达创建订单成功之后才来查询，或者等待一段时间后重试<br/>
> 那么如何知道订单创建成功？可以参考[订单回调](http://newopen.imdada.cn/#/development/file/order),<br/> 
> 即在收到达达回调order_status=1时说明达达内部创建订单成功。

#### 对接达达的准备工作
开发者准备工作：
1. 创建开发者账号
2. 开发者对接达达相关接口
3. 开发者在达达QA环境联调
4. 生产环境：开发者和商户进行绑定
5. 进行发单

商户准备工作
1. 创建商户账号
2. 商户创建门店
3. 商户充值
4. 商户跟开发者绑定

#### 达达开放平台环境说明(http和https都支持)
- 测试域名：newopen.qa.imdada.cn
- 线上域名：newopen.imdada.cn

#### 如何QA环境联调
在达达QA环境(newopen.qa.imdada.cn)联调时，需要修改 `com.imdada.open.platform.config.Configuration` 中的配置参数。

默认情况下，每个开发者再达达生产环境(newopen.imdada.cn)注册之后，我们都会为其在QA环境生成一套联调的数据。

开发者可以在 [应用信息](http://newopen.imdada.cn/#/developer/help/appInfo) (需要登录开发者账号)页面查看相关信息

具体来说：
- host 配置为`http://newopen.qa.imdada.cn`
- callback 配置为自己的回调地址
- sourceId 配置为应用信息页面-联调商户-商户SourceID
- shopNo 配置为应用信息页面-联调商户-门店编号
- appKey 配置为应用信息页面的 app_key
- appSecret 配置为应用信息页面的 app_sercret

配置好这些参数之后，正常应该就可以成功调用相关api接口了

如果需要流转订单(骑士接单、到店、取货、送达等)，
可以借助开发者[联调工具](http://newopen.imdada.cn/#/developer/help/jointDebuggerTools)(需要登录开发者账号)

通过联调工具走一遍订单的正向配送流程：
- 新增订单(可以通过联调工具，也可以通过调用api接口发单)
- 骑士接单
- 骑士到店
- 骑士取货
- 骑士完成

可以在[日志查询](http://newopen.imdada.cn/#/developer/help/callback)里面查询请求和回调日志

异常妥投流程：
- 新增订单(可以通过联调工具，也可以通过调用api接口发单)
- 骑士接单
- 骑士到店
- 骑士取货
- 骑士取货后异常上报(商家原因-不审核)
- 妥投异常-确认收到物品

可以在[日志查询](http://newopen.imdada.cn/#/developer/help/callback)里面查询请求和回调日志

商家取消订单流程：
- 新增订单(可以通过联调工具，也可以通过调用api接口发单)
- 商户取消订单

以及骑士原因取消重抛等场景，都可以通过联调工具来自助完成

#### 包说明
- com.imdada.open.platform.client.config   达达开放平台发单相关的配置内容(正常来说整个应用只需要一个实例即可)
- com.imdada.open.platform.client.order   【订单管理】的client示例
- com.imdada.open.platform.client.shop    【商户管理】的client示例
- com.imdada.open.platform.client.message 【消息通知】的client示例
- com.imdada.open.platform.client.allinone.example.shop  可以拷贝后直接运行的all-in-one代码示例
- com.imdada.open.platform.callback        达达状态回调示例
