
接口请求参数示例：
```json
{
	"app_key": "dada3af1e313b0d1c9a",
	"signature": "67C8565068CD3CA378539D4701B7BF3E",
	"v": "1.0",
	"format": "json",
	"source_id": "73753",
	"body": "{\"amount\":\"0.01\",\"category\":\"H5\"}",
	"timestamp": 1663843665
}
```
其中body序列化之前的示例：
```json
{
	"amount": "0.01",
	"category": "H5"
}
```

返回结果示例：
```json
{
	"status": "success",
	"result": "https://daojia.jd.com/html/index.html?payToken=xxx",
	"code": 0,
	"msg": "成功",
	"success": true,
	"fail": false
}
```
